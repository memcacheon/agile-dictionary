package cloud.agileframework.dictionary.cache;

/**
 * 缓存区域
 */
public enum RegionEnum {
    //以fullCode为key的缓存数据
    CODE_MEMORY,
    //以fullName为key的缓存数据
    NAME_MEMORY,
    ID_MEMORY,
    FULL_ID_MEMORY
}
