package cloud.agileframework.dictionary;

import cloud.agileframework.cache.util.BeanUtil;
import cloud.agileframework.common.constant.Constant;
import cloud.agileframework.dictionary.cache.DictionaryCache;
import cloud.agileframework.dictionary.cache.MemoryCacheImpl;
import cloud.agileframework.dictionary.cache.NotFoundCacheException;

import java.util.List;

/**
 * @author 佟盟
 * 日期 2020/7/30 19:44
 * 描述 字典数据管理器
 * @version 1.0
 * @since 1.0
 */
public interface DictionaryDataManager<D extends DictionaryDataBase> {
    /**
     * 获取所有字典数据
     *
     * @return 字典数据集合
     */
    List<D> all();

    /**
     * 唯一的数据源标识
     *
     * @return 用于Convert注解中dataSource声明，将数据按照数据源标识划分成不同存储区域
     */
    default String dataSource() {
        return Constant.AgileAbout.DIC_DATASOURCE;
    }

    /**
     * 初始化字典缓存介质
     *
     * @return 多次调用必须返回同一个对象
     */
    default DictionaryCache cache() {
        return MemoryCacheImpl.INSTANT;
    }

    /**
     * 取根节点数据的parentId值
     *
     * @return 默认返回空
     */
    default String rootParentId() {
        return null;
    }

    default void refresh() throws NotFoundCacheException {
        BeanUtil.getApplicationContext().getBean(DictionaryEngine.class).refresh(this);
    }
}
